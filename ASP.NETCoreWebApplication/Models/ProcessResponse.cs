namespace ASP.NETCoreWebApplication.Models
{
	public class ProcessResponse
	{
		public int Pid { get; set; }
		public string Name { get; set; }
		public bool Attached { get; set; }
	}
}