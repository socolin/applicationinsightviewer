using System.Collections.Generic;
using ASP.NETCoreWebApplication.Models;
using ASP.NETCoreWebApplication.Services;
using Microsoft.AspNetCore.Mvc;

namespace ASP.NETCoreWebApplication.Controllers
{
	[Route("api/[controller]")]
	public class ProcessesController : Controller
	{
		private readonly ProcessFinderService _processFinderService;
		private readonly ApplicationInsightLoggerReaderService _applicationInsightLoggerReaderService;

		public ProcessesController(
			ProcessFinderService processFinderService,
			ApplicationInsightLoggerReaderService applicationInsightLoggerReaderService
		)
		{
			_processFinderService = processFinderService;
			_applicationInsightLoggerReaderService = applicationInsightLoggerReaderService;
		}

		[HttpGet]
		public IList<ProcessResponse> Get()
		{
			return _processFinderService.ListProcesses();
		}


		[HttpPost("{Pid}/Attach")]
		public IActionResult Attach(int pid)
		{
			_applicationInsightLoggerReaderService.AttachProcess(pid);
			return Ok();
		}

		[HttpPost("{Pid}/Detach")]
		public IActionResult Detach(int pid)
		{
			_applicationInsightLoggerReaderService.DetachProcess(pid);
			return Ok();
		}
	}
}