import {Component} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ProcessManagerComponent} from './logs/process-manager.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public dialog: MatDialog) {
  }

  openSelectProcess() {
    this.dialog.open(ProcessManagerComponent, {
      width: '700px'
    });
  }
}
