import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatDividerModule,
  MatFormFieldModule, MatInputModule,
  MatListModule
} from '@angular/material';
import {LogsComponent} from './logs/logs.component';
import {ProcessManagerComponent} from './logs/process-manager.component';
import {ProcessService} from './logs/process.service';

@NgModule({
  declarations: [
    AppComponent,
    LogsComponent,
    ProcessManagerComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'ng-cli-universal'}),
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    RouterModule.forRoot([
      {path: '', component: LogsComponent, pathMatch: 'full'},
      {path: 'logs', component: LogsComponent}
    ]),
    MatButtonModule,
    MatDividerModule,
    MatCheckboxModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule
  ],
  providers: [ProcessService],
  bootstrap: [AppComponent],
  entryComponents: [ProcessManagerComponent]
})
export class AppModule {
}
