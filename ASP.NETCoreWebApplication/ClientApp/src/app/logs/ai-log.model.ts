export class AILog {
  name: string;
  time: string;
  iKey: string;
  tags: { [tagName: string]: string };
  data: {
    baseData: {
      ver: number;
      name: string;
      properties?: { [propertyName: string]: string };
    }
  };

  get type(): string {
    return this.name.substr(this.name.lastIndexOf('.') + 1);
  }

  get hasAnalyticErrors(): boolean {
    return this.data.baseData.properties && 'AnalyticErrors' in this.data.baseData.properties;
  }

  get appName(): string | undefined {
    if (!this.data.baseData.properties)
      return undefined;
    const rawAppName = this.data.baseData.properties['AppName'];
    if (!rawAppName)
      return undefined;
    return rawAppName.substr(rawAppName.lastIndexOf('.') + 1);
  }

  get operationName(): string | undefined {
    return this.tags["ai.operation.name"] || this.data.baseData.name;
  }

  get storeId(): string | undefined {
    if (!this.data.baseData.properties)
      return undefined;
    return this.data.baseData.properties['StoreId'];
  }
}
