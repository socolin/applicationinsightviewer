import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {ProcessService} from './process.service';
import {Process} from './process.model';

@Component({
  selector: 'app-process-manager-component',
  styleUrls: ['./process-manager.component.scss'],
  templateUrl: './process-manager.component.html'
})
export class ProcessManagerComponent implements OnInit {
  public processes: Process[];
  public filter: string;

  constructor(
    public dialogRef: MatDialogRef<ProcessManagerComponent>,
    private processService: ProcessService
  ) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.processService.listProcess().subscribe(processes => this.processes = processes);
  }

  selectProcess(process: Process) {
    if (process.attached)
      this.processService.detachProcess(process.pid).subscribe(x => this.dialogRef.close());
    else
      this.processService.attachProcess(process.pid).subscribe(x => this.dialogRef.close());
    process.attached = !process.attached;
  }

  matchFilter(process): boolean {
    if (!this.filter)
      return true;
    return process.name.indexOf(this.filter) !== -1
  }
}
