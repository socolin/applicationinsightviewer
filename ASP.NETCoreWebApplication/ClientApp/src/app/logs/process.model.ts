export class Process {
  pid: number;
  name: string;
  attached: boolean;
}
