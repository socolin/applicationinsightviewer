import { Component } from '@angular/core';
import { HubConnection } from '@aspnet/signalr';
import * as signalR from '@aspnet/signalr';
import {AILog} from './ai-log.model';

@Component({
  selector: 'app-logs-component',
  styleUrls: ['./logs.component.scss'],
  templateUrl: './logs.component.html'
})
export class LogsComponent {
  private _hubConnection?: HubConnection;
  public logs: AILog[] = [];
  public displayedTypes: {[typeName: string]: boolean} = {};
  public selectedLog?: AILog = undefined;
  public objectKeys = Object.keys;

  constructor() {
  }

  clear() {
    this.logs = [];
  }

  selectLog(log: AILog) {
    this.selectedLog = log;
  }

  ngOnInit() {
    this._hubConnection = new signalR.HubConnectionBuilder()
      .withUrl('https://localhost:5001/logger')
      .configureLogging(signalR.LogLevel.Information)
      .build();

    this._hubConnection.start().catch(err => console.error(err.toString()));

    this._hubConnection.on('log', (data: string) => {
      const log = new AILog();
      Object.assign(log, JSON.parse(data));

      if (!(log.type in this.displayedTypes)) {
        this.displayedTypes[log.type] = true;
      }

      this.logs.push(log);
    });
  }
}
