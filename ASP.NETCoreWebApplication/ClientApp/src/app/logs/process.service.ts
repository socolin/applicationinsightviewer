import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Process} from './process.model';

@Injectable()
export class ProcessService {
  constructor(private httpClient: HttpClient) {
  }

  public listProcess(): Observable<Process[]> {
    return this.httpClient.get<Process[]>('api/processes');
  }

  public attachProcess(processId: any) {
    return this.httpClient.post('api/processes/' + processId + '/attach', null);
  }

  public detachProcess(processId: any) {
    return this.httpClient.post('api/processes/' + processId + '/detach', null);
  }
}
