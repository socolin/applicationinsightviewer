using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using ASP.NETCoreWebApplication.Exceptions;
using ASP.NETCoreWebApplication.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Samples.Debugging.CorDebug;
using Microsoft.Samples.Debugging.MdbgEngine;

namespace ASP.NETCoreWebApplication.Services
{
	public class ApplicationInsightLoggerReaderService : IDisposable
	{
		private readonly IHubContext<LoggerHub> _hubContext;
		private readonly MDbgEngine _mDbgEngine = new MDbgEngine();
		private MDbgProcess _process;
		private string _aiMsgPrefix;
		private ManualResetEvent _stop = new ManualResetEvent(false);

		public ApplicationInsightLoggerReaderService(IHubContext<LoggerHub> hubContext)
		{
			_hubContext = hubContext;
		}

		public int AttachedProcessId => _process?.CorProcess.Id ?? 0;

		public void DetachProcess(int pid)
		{
			if (_process?.CorProcess.Id != pid)
				return;

			_process?.AsyncStop().WaitOne();
			_process?.Detach();
			_process = null;
		}

		public void AttachProcess(int pid)
		{
			if (_process != null) // FIXME: manage a list of process
				throw new AlreadyAttachedException();

			Task.Factory.StartNew(() =>
			{

				try
				{
					Thread.Sleep(1000); // Workaround overlap IO exception
					_process = _mDbgEngine.Attach(pid);
					_process.Go().WaitOne();
					_process.Go();

					_process.PostDebugEvent +=
						(sender, e) =>
						{
							try
							{
								if (e.CallbackType == ManagedCallbackType.OnBreakpoint)
									_process.Go().WaitOne();

								if (e.CallbackType == ManagedCallbackType.OnLogMessage)
								{
									var message = ((CorLogMessageEventArgs) e.CallbackArgs).Message;
									_aiMsgPrefix = "Application Insights Telemetry: ";
									if (message.StartsWith(_aiMsgPrefix))
									{
										message = message.Substring(_aiMsgPrefix.Length);
										_hubContext.Clients.All.SendCoreAsync("log", new object[] {message});
									}
								}

								if (e.CallbackType == ManagedCallbackType.OnProcessExit)
									Dispose();
							}
							catch (Exception ex)
							{
								// FIXME: log
								Console.WriteLine(ex);
							}
						};

					_stop.WaitOne();
				}
				catch (Win32Exception ex)
				{
					// FIXME: log
					Console.WriteLine(ex);
				}
			});
		}

		public void Dispose()
		{
			_stop.Set();
			_process?.AsyncStop().WaitOne();
			_process?.Detach();
			_process = null;
		}
	}
}