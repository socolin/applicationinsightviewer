using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ASP.NETCoreWebApplication.Models;

namespace ASP.NETCoreWebApplication.Services
{
	public class ProcessFinderService
	{
		private readonly ApplicationInsightLoggerReaderService _applicationInsightLoggerReaderService;

		public ProcessFinderService(ApplicationInsightLoggerReaderService applicationInsightLoggerReaderService)
		{
			_applicationInsightLoggerReaderService = applicationInsightLoggerReaderService;
		}

		public List<ProcessResponse> ListProcesses()
		{
			return Process.GetProcesses()
				.Select(p => new ProcessResponse
				{
					Pid = p.Id,
					Name = p.ProcessName,
					Attached = _applicationInsightLoggerReaderService.AttachedProcessId == p.Id
				}).ToList();
		}
	}
}