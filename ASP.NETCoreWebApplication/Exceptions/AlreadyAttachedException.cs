using System;

namespace ASP.NETCoreWebApplication.Exceptions
{
	public class AlreadyAttachedException : Exception
	{
		public AlreadyAttachedException()
		{
		}
	}
}